extern crate log;

use std::{
    collections::HashMap,
    convert::TryFrom,
    env,
    fmt,
    ffi::OsStr,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct LogConfig<'a> {
    base: Option<&'a str>,
    pub level: log::Level,
}

impl<'a> LogConfig<'a> {
    #[inline(always)]
    pub fn global(level: log::Level) -> Self {
        LogConfig {
            base: None,
            level: level,
        }
    }

    #[inline]
    pub const fn new(base: Option<&'a str>, level: log::Level) -> Self {
        LogConfig {
            base: base,
            level: level,
        }
    }

    #[inline(always)]
    pub fn base(&self) -> Option<&'a str> {
        self.base
    }
}

impl<'a> TryFrom<&'a str> for LogConfig<'a> {
    type Error = log::ParseLevelError;

    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        if let Some((base, level)) = s.split_once('=') {
            Ok(LogConfig {
                base: Some(base),
                level: level.parse::<log::Level>()?,
            })
        } else {
            s.parse::<log::Level>()
                .map(LogConfig::global)
        }
    }
}

impl<'a> fmt::Display for LogConfig<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(base) = self.base.as_ref() {
            write!(f, "{}=", base)?;
        }
        write!(f, "{}", self.level)
    }
}

pub struct LogConfigList<'a> {
    entries: HashMap<Option<&'a str>, log::Level>,
}

impl<'a> LogConfigList<'a> {
    pub fn parse(s: &'a str) -> Result<Self, log::ParseLevelError> {
        let mut ret: HashMap<Option<&'a str>, log::Level> = HashMap::new();
        s.split(',')
            .map(LogConfig::try_from)
            .fold(Ok(&mut ret), |r, v| r.and_then(move |r| v.map(move |v| {
                r.insert(v.base, v.level);
                r
            })))?;
        Ok(LogConfigList {
            entries: ret,
        })
    }

    pub fn entries<'s>(&'s self) -> impl Iterator<Item=LogConfig<'a>> + 's {
        self.entries.iter()
            .map(|(&k, &l)| LogConfig::new(k, l))
    }

    pub fn get_config(&self, name: &'a str) -> Option<LogConfig<'_>> {
        self.entries.get_key_value(&Some(name))
            .or_else(|| self.entries.get_key_value(&None))
            .map(|(&base, &level)| LogConfig::new(base, level))
    }

    pub fn default_config(&self) -> Option<log::Level> {
        self.entries.get(&None)
            .map(|&v| v)
    }

    pub fn has_config(&self, name: &str) -> bool {
        self.get_config(name).is_some()
    }

    pub fn store<K>(&self, key: K)
    where
        K: AsRef<OsStr>,
    {
        env::set_var(key, format!("{}", self));
    }

    #[inline(always)]
    pub fn insert(&mut self, base: Option<&'a str>, level: log::Level) -> Option<log::Level> {
        self.entries.insert(base, level)
    }

    #[inline(always)]
    pub fn insert_config(&mut self, item: LogConfig<'a>) -> Option<log::Level> {
        self.insert(item.base, item.level)
    }
}

impl<'a> TryFrom<Option<&'a str>> for LogConfigList<'a> {
    type Error = log::ParseLevelError;
    fn try_from(s: Option<&'a str>) -> Result<Self, Self::Error> {
        if let Some(s) = s.into_iter().filter(|s| s.len() > 0).next() {
            Self::parse(s)
        } else {
            Ok(Default::default())
        }
    }
}

impl<'a> Default for LogConfigList<'a> {
    fn default() -> Self {
        LogConfigList {
            entries: HashMap::new(),
        }
    }
}

fn write_separated<S, It>(it: It, separator: &S, f: &mut fmt::Formatter<'_>) -> fmt::Result
where
    It: IntoIterator,
    <It as IntoIterator>::Item: fmt::Display,
    S: fmt::Display,
{
    let mut it = it.into_iter();
    if let Some(fst) = it.next() {
        write!(f, "{}", fst)?;
    }
    for v in it {
        write!(f, "{}{}", separator, v)?;
    }
    Ok(())
}

impl<'a> fmt::Display for LogConfigList<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write_separated(self.entries(), &',', f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use log::Level;

    #[test]
    fn specific_parses() {
        const SPECIFIC_CONFIGS: [&'static str; 4] = [
            "foo=DEBUG",
            "foo=debug",
            "foo=INFO",
            "foo=info"
        ];
        SPECIFIC_CONFIGS.iter().map(|&v| v).for_each(|s| {
            let cfg = LogConfigList::try_from(Some(s)).expect("failed to parse config");
            assert!(cfg.has_config("foo"));
        });
    }

    #[test]
    fn multiple_parses() {
        let cfg = LogConfigList::parse("foo=info,bar=debug").expect("failed to parse config");
        let foo_cfg = cfg.get_config("foo").expect("expected foo config");
        assert_eq!(foo_cfg.base(), Some("foo"));
        assert_eq!(foo_cfg.level, Level::Info);
        let bar_cfg = cfg.get_config("bar").expect("expected bar config");
        assert_eq!(bar_cfg.base(), Some("bar"));
        assert_eq!(bar_cfg.level, Level::Debug);
    }

    #[test]
    fn default_parses() {
        let config = LogConfigList::parse("info,foo=debug").expect("failed to parse config");
        let mut cfg = config.get_config("foo").expect("expected foo config");
        assert_eq!(cfg, LogConfig::new(Some("foo"), Level::Debug));
        cfg = config.get_config("bar").expect("expected bar config");
        assert!(cfg.base().is_none());
        assert_eq!(cfg.level, Level::Info);
    }

    #[test]
    fn serialize_deserialize() {
        const SERDE_TEST: &'static str = "foo=DEBUG";
        let mut cfg = LogConfigList::parse(SERDE_TEST).expect("failed to parse config");
        assert_eq!(format!("{}", &cfg), SERDE_TEST);
        cfg.insert(Some("bar"), Level::Info);
        let mut serialized = format!("{}", &cfg);
        assert!(&serialized == "foo=DEBUG,bar=INFO" || &serialized == "bar=INFO,foo=DEBUG");
        assert_eq!(serialized.split(',').count(), 2);
        cfg.insert(None, Level::Warn);
        serialized = format!("{}", &cfg);
        assert_eq!(serialized.split(',').count(), 3);
    }
}
